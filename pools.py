import os
import time

from flask import request
from flask import Flask, render_template, Response
import mysql.connector
from mysql.connector import errorcode
import requests
import simplejson as json

application = Flask(__name__)
app = application


#INITIALIZATION OF CREDIENTIALS TO ACCESS MYSQL DB
def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def instantiate_mysql_cnx():
	db, username, password, hostname = get_db_creds()
	cnx = ''
	try:
		cnx = mysql.connector.connect(user=username, password=password, host=hostname, database=db)
	except Exception as exp:
		print(exp)
		import MySQLdb
        #try:
		cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        #except Exception as exp1:
        #    print(exp1)
	if cnx:
		return cnx
	else:
		return "Error in building MySQL connection"


#METHOD FOR CREATING TABLE IN DATABASE
def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
	cnx = instantiate_mysql_cnx()
	table_ddl = 'CREATE TABLE poolinfo(pool_name VARCHAR(200), status VARCHAR(50), phone VARCHAR(20), pool_type VARCHAR(20), PRIMARY KEY (pool_name))'
    
	cur = cnx.cursor()
	try:
		cur.execute(table_ddl)
		cnx.commit()
	except mysql.connector.Error as err:
		if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
			print("already exists.")
		else:
			print(err.msg)


@app.route('/')
def home():
	return 'Welcome to Austin Pool Information Website.'


@app.route('/pools', methods=['POST'])
def post_pool():
	db, username, password, hostname = get_db_creds()

	print("Posting new pool to DB")
	print("DB: %s" % db)
	print("Username: %s" % username)
	print("Password: %s" % password)
	print("Hostname: %s" % hostname)
	post_data = request.json
	cnx = instantiate_mysql_cnx()

	pool_name = post_data["pool_name"]
	status = post_data["status"]
	phone =  post_data["phone"]
	pool_type =  post_data["pool_type"]
	validity = valid_input('POST', pool_name, status, phone, pool_type)

	if validity[0]:
		cur = cnx.cursor()
		sql = "INSERT INTO poolinfo (pool_name, status, phone, pool_type) VALUES (%s, %s, %s, %s)"
		val = (pool_name, status, phone, pool_type)
		cur.execute(sql, val)
		cnx.commit()

		print("POST request complete")
		return Response(status="201 created")
	else:
		temp = {'Message':validity[1]}
		r = json.dumps(temp)
		return Response(response=r, status='400 Bad Data')


@app.route('/pools/<pool_name>', methods=['GET', 'PUT', 'DELETE'])
def get_handler(pool_name):
	if request.method == 'GET':
		return get_pool(pool_name)
	elif request.method == 'PUT':
		post_data = request.json
		pool_name = post_data["pool_name"]
		status = post_data["status"]
		phone =  post_data["phone"]
		pool_type =  post_data["pool_type"]
		validity = valid_input('PUT', pool_name, status, phone, pool_type)

		if validity[0]:
			return update_pool(pool_name, status, phone, pool_type)
		else:
			if 'does not exist' in validity[1]:
				temp = {'Message':validity[1]}
				r = json.dumps(temp)
				return Response(response=r, status='404 Not Found')
			else:
				temp = {'Message':validity[1]}
				r = json.dumps(temp)
				return Response(response=r, status='400 Bad Data')
	else:
		return delete_pool(pool_name)


def valid_input(method, pool_name, status, phone, pool_type):
	valid = True

	#	CHECK POOL_NAME IN TABLE
	db, username, password, hostname = get_db_creds()
	cnx = instantiate_mysql_cnx()
	cur = cnx.cursor()
	sql = "SELECT pool_name FROM poolinfo WHERE pool_name = %s"
	val = (pool_name,)
	cur.execute(sql, val)
	record = cur.fetchone()
	if record:
		if method=='POST':
			message = 'Pool with name %s already exists' %(record[0])
			return [not valid, message]
	else:
		if method=='PUT':
			message = 'Pool with name %s does not exist' %(pool_name)
			return [not valid, message]

	#	CHECK STATUS
	if status not in ["Closed", "Open", "In Renovation"]:
		if method=='POST':
			message = 'status field has invalid value'
			return [not valid, message]
		else:
			message = 'Update to %s field not allowed' %(pool_name)
			return [not valid, message]

	#	CHECK PHONE FORMAT
	for i in range(len(list(phone))):
		if (i == 3 or i == 7):
			if phone[i] != '-':
				if method=='POST':
					message = 'phone field not in valid format'
					return [not valid, message]
				else:
					message = 'Update to %s field not allowed' %(pool_name)
					return [not valid, message]
		else:
			if not phone[i].isdigit():
				if method=='POST':
					message = 'phone field not in valid format'
					return [not valid, message]
				else:
					message = 'Update to %s field not allowed' %(pool_name)
					return [not valid, message]
	
	#	CHECK POOL_TYPE
	if pool_type not in ["Neighborhood", "University", "Community"]:
		if method=='POST':
			message = 'pool_type field has invalid value'
			return [not valid, message]
		else:
			message = 'Update to %s field not allowed' %(pool_name)
			return [not valid, message]

	return [valid, ""]


def get_pool(pool_name):
	db, username, password, hostname = get_db_creds()
	cnx = instantiate_mysql_cnx()
	cur = cnx.cursor()
	sql = "SELECT * FROM poolinfo WHERE pool_name = %s"
	val = (pool_name,)
	cur.execute(sql, val)
	record = cur.fetchone()
	if record:
		output = {'pool_name':record[0], 'status':record[1], 'phone':record[2], 'pool_type':record[3]}
		r = json.dumps(output)
		print('GET request complete')
		return Response(response=r, status='200 OK')
	else:
		output = {'Message': 'Pool with name %s does not exist' %(pool_name)}
		r = json.dumps(output)
		return Response(response=r, status='404 Not Found')


def update_pool(pool_name, status, phone, pool_type):
	db, username, password, hostname = get_db_creds()
	cnx = instantiate_mysql_cnx()
	cur = cnx.cursor()
	sql = "UPDATE poolinfo SET  status=%s, phone=%s, pool_type=%s WHERE pool_name = %s"
	val = (status, phone, pool_type, pool_name)
	cur.execute(sql, val)
	cnx.commit()
	print('PUT request complete')

	output = {'pool_name':pool_name, 'status':status, 'phone':phone, 'pool_type':pool_type}
	r = json.dumps(output)
	return Response(response=r, status='200 OK')


def delete_pool(pool_name):
	db, username, password, hostname = get_db_creds()
	cnx = instantiate_mysql_cnx()
	cur = cnx.cursor()
	sql = "SELECT * FROM poolinfo WHERE pool_name = %s"
	val = (pool_name,)
	cur.execute(sql, val)
	record = cur.fetchone()
	if record:
		sql = "DELETE FROM poolinfo WHERE pool_name = %s"
		val = (pool_name,)
		cur.execute(sql, val)
		cnx.commit()
		print('DELETE request complete')
		return Response(status="200 OK")
	else:
		output = {'Message': 'Pool with name %s does not exist' %(pool_name)}
		r = json.dumps(output)
		return Response(response=r, status='404 Not Found')


try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table global")
    create_table()
    print("After create_data global")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


if __name__ == "__main__":
    app.debug = True
    app.run()



